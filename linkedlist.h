#include <stdlib.h>

#include "macro_concat.h"

#ifndef TYPE
#define DEFAULTED_TO_DOUBLE
#define TYPE double
#endif

#define STRUCT(NAME) MCAT(MCAT(NAME, _),TYPE)

#define FUNCTION(NAME) MCAT(MCAT(MCAT(LinkedList_,TYPE),_),NAME)

typedef struct STRUCT(LinkedListNode)
{
    TYPE value;
    struct STRUCT(LinkedListNode) * next;
} STRUCT(LinkedListNode);

typedef struct STRUCT(LinkedList)
{
    unsigned long size;
    struct STRUCT(LinkedListNode) * first;
} STRUCT(LinkedList);

STRUCT(LinkedList) * FUNCTION(create)()
{
    STRUCT(LinkedList) *this = (STRUCT(LinkedList) *)malloc(sizeof(STRUCT(LinkedList)));
    this->size = 0lu;
    this->first = NULL;
    return this;
}

void FUNCTION(destroy)(STRUCT(LinkedList) * this)
{
    while (this->first != NULL)
    {
        STRUCT(LinkedListNode) *tmp = this->first;
        this->first = this->first->next;
        free(tmp);
    }
    free(this);
}

void FUNCTION(push_front)(STRUCT(LinkedList) * this, TYPE value)
{
    ++this->size;
    STRUCT(LinkedListNode) *new = (STRUCT(LinkedListNode) *)malloc(sizeof(STRUCT(LinkedListNode)));
    new->value = value;
    new->next = this->first;
    this->first = new;
}

TYPE FUNCTION(pop_first)(STRUCT(LinkedList) * this)
{
    --this->size;
    TYPE value = this->first->value;
    STRUCT(LinkedListNode) *tmp = this->first;
    this->first = this->first->next;
    free(tmp);
    return value;
}

TYPE FUNCTION(at)(STRUCT(LinkedList) * this, unsigned long index)
{
    STRUCT(LinkedListNode) *tmp = this->first;
    for (unsigned long i = 0lu; i < index; ++i)
    {
        tmp = tmp->next;
    }
    return tmp->value;
}

#undef STRUCT
#undef FUNCTION

#ifdef DEFAULTED_TO_DOUBLE
#undef DEFAULTED_TO_DOUBLE
#undef TYPE
#endif

