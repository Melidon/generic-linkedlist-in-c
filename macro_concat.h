#ifndef MACRO_CONCAT_H
#define MACRO_CONCAT_H

#define _MCAT(a, b) a##b

#define MCAT(a, b) _MCAT(a, b)

#endif

