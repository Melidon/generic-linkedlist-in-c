#include <stdio.h>

#define TYPE int
#include "linkedlist.h"
#undef TYPE

#define TYPE double
#include "linkedlist.h"
#undef TYPE

int main()
{
    LinkedList_int *list_int = LinkedList_int_create();
    LinkedList_int_push_front(list_int, 1);
    LinkedList_int_push_front(list_int, 2);
    printf("%d\n", LinkedList_int_pop_first(list_int));
    printf("%d\n", LinkedList_int_pop_first(list_int));
    LinkedList_int_destroy(list_int);

    printf("\n");

    LinkedList_double *list_double = LinkedList_double_create();
    LinkedList_double_push_front(list_double, 1.0);
    LinkedList_double_push_front(list_double, 2.0);
    printf("%f\n", LinkedList_double_pop_first(list_double));
    printf("%f\n", LinkedList_double_pop_first(list_double));
    LinkedList_double_destroy(list_double);
    return 0;
}
